@extends('admin.layouts.master')
@section('content')


<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
    Company Management
    </h1>
  </section>
  <!-- Main content -->
  <section class="content">
    <!--tab content -->
    <!-- Small boxes (Stat box) -->
   

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
                    @if(Session::has('message'))
                    <div class="alert {{ Session::get('alert-class', 'alert-success') }} ">
                    <div style="display:inline-block" id="">
                    {{ Session::get('message') }}
                    </div>
                    <a href="#" class="close" data-dismiss="alert" aria-label="close" style="display:inline-block">&times;</a>
                    </div>
                    @endif
              <h3 class="box-title"></h3>
            </div>
            <div class="col-sm-12">
              <a href="{{ url('admin/company-add')}}" class="btn btn-success pull-right">Add Company</a>
            </div>
            <!-- /.box-header -->
            <div class="box-body" style="overflow-x: auto">
              <table id="example2" class="table table-bordered table-hover">
                <thead>
                  <tr>
                    <th>ID</th>
                    <th>Company Name</th>
                    <th>Email</th>
                    <th>Website Url</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                   @forelse($companies as $key =>$data)
                  <tr>
                   <td>{{$key+1}}</td> 
                   <td>{{ $data->name }}</td> 
                   <td>{{ $data->email }}</td> 
                   <td>{{ $data->website_url}}</td> 
                   <td>
                    <a href="{{ url('admin/company-view/'.$data->id) }}" class="btn btn-primary" title="View"><i class="text-white fa fa-eye" aria-hidden="true"></i></a>
                    <a href="{{ url('admin/company-edit/'.$data->id) }}" class="btn btn-primary" title="Edit"><i class="text-white fa fa-pencil-square-o" aria-hidden="true"></i></a>
                    <a href="#" onclick="delete_company('{{$data->id}}')" class="btn btn-danger" title="Delete"><i class="text-white fa fa-trash" aria-hidden="true"></i></a>
                   </td>  
                   
                  </tr>
                 @empty
                  <tr class="row">
                    <td colspan="3" class="text-center">
                      {{ 'No Data Found!' }}
                    </td>
                  </tr>
            @endforelse
                </tbody>
              </table>
              <center></center>
              {{$companies->appends(request()->except('page'))->links()}}
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
      </div>
    </section>
  </section>
  <!-- /.content-wrapper -->
</div>
@endsection
@section('scripts')
<script type="text/javascript">
  var public_url = $('meta[name="base_url"]').attr('content');
  //Function to delete template
  function delete_company(id) {
    swal({
      title: "Are you sure?",
      text: "Are you sure you want to delete the company?",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#069edb",
      confirmButtonText: "Yes, delete it!",
      closeOnConfirm: false
    }, function() {
      
      $.ajaxSetup({
        headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
      });
      $.ajax({
        type: 'get',
        data: {
        '_method': 'get'
        },
        url: public_url + "/admin/company-delete/" +id,
        success: function(data) {
          swal({
            title: " Success!",
            text: "Company is successfully deleted!",
            type: "success",
            confirmButtonColor: "#069edb",
            //timer: 3000
          },
          function() {
            window.location.href = public_url + '/admin/company-list';
          });
        }
      });
    });
  }
</script>
@endsection