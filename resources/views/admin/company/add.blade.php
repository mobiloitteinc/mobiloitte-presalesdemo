@extends('admin.layouts.master')
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
        Add Company
        </h1>
    </section>
    <!-- Main content -->
    <section class="content">
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <div class="box-header">
                            <h2 class="box-title"></h2>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                            @endif
                            <div class="container">
                                <div class="row">
                                    <div class="col-sm-10">
                                        <form method="POST" action="{{url('admin/company-store')}}" id="company_add" enctype="multipart/form-data">
                                            {{ csrf_field() }}
                                            <div class="profileView bgWhite mb30 ">
                                                <div class="formBox max-WT-600 center-box">
                                                    <br>
                                                    <div class="profile-image-block">
                                                        <img class="img-responsive avatar-view img-circle" id="blah" src="http://172.16.0.101/PROJECTS/ClassPass/trunk/public/Admin/dist/img/user2-160x160.jpg" alt="Avatar" title="Change the avatar">
                                                        <label title="Upload Image" class="camera-icon">
                                                            <div class="cam_icon">
                                                                <input type="file" onchange="readURL(this);" name="image">
                                                                <i class="fa fa-camera"></i>
                                                            </div>
                                                        </label>
                                                    </div>
                                                    <div class="form-group row align-items-center flex-wrap">
                                                        <label class="col-md-4 col-form-label">Company Name</label>
                                                        <div class="col-md-8 toolTipCol">
                                                            <input type="text" class="form-control" name="company_name" value="{{old('company_name')}}" maxlength="199" placeholder="Enter Company Name" required>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row align-items-center flex-wrap {{ $errors->has('company_name') ? ' has-error' : '' }}">
                                                    <div class="form-group row align-items-center flex-wrap">
                                                        <label class="col-md-4 col-form-label">Company Email</label>
                                                        <div class="col-md-8 toolTipCol">
                                                            <input type="text" class="form-control" name="company_email" value="{{old('company_email')}}" maxlength="200" placeholder="Enter Company Email" required>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row align-items-center flex-wrap {{ $errors->has('company_email') ? ' has-error' : '' }}">
                                                        <label class="col-md-4 col-form-label">Website Url</label>
                                                        <div class="col-md-8 toolTipCol">
                                                            <input type="text" class="form-control" name="website_url" value="{{old('website_url')}}" maxlength="200" placeholder="Enter Company Url" required>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row align-items-center flex-wrap {{ $errors->has('website_url') ? ' has-error' : '' }}">
                                                </div>
                                                <div class="text-center">
                                                    <button class="btn btn-primary max-WT-180" type="submit">Add</button>
                                                    <a href="{{ url('admin/company-list') }}" class="btn btn-danger">Cancel</a>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                        <!-- /.box-body -->
                    </div>
                </div>
                <!-- /.box -->
            </div>
        </section>
        
    </section>
    <!-- /.content-wrapper -->
</div>
@endsection
@section('scripts')
<script type="text/javascript">
$("#company_add").validate({
errorElement: "span",
wrapper: "span",
errorPlacement: function(error, element) {
offset = element.offset();
error.insertAfter(element)
error.css('color','red');
},
rules: {
},
messages: {
company_name:{
required:"*Please enter company name."
},
company_email:{
required:"*Please enter email.",
email:"*Invalid email."
},
},
});
function readURL(input) {
if (input.files && input.files[0]) {
var reader = new FileReader();
reader.onload = function(e) {
$('#blah')
.attr('src', e.target.result)
.width(100)
.height(100);
};
reader.readAsDataURL(input.files[0]);
}
}
</script>
@endsection