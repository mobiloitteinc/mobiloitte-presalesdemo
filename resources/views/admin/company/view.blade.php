@extends('admin.layouts.master')
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
        View Company
        </h1>
    </section>
    <!-- Main content -->
    <section class="content">
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <div class="box-header">
                            <h2 class="box-title"></h2>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <div class="container">
                                <div class="row">
                                    <div class="col-sm-10">
                                        <div class="profileView bgWhite mb30 ">
                                            <br>
                                            <div class="form-group row align-items-center flex-wrap">
                                                @if(isset($company->logo) ? $company->logo : '')
                                                <label class="col-md-4 col-form-label">Company Logo:</label>
                                                <img class="profile-user-img img-responsive img-circle" src="{{ asset('storage/'.$company->logo) }}" alt="User profile picture">
                                                @endif
                                            </div>
                                            <div class="form-group row align-items-center flex-wrap">
                                                <label class="col-md-4 col-form-label">Company Name:</label>
                                                <div class="col-md-8 toolTipCol">
                                                    {{$company->name}}
                                                </div>
                                            </div>
                                            <div class="form-group row align-items-center flex-wrap">
                                                <label class="col-md-4 col-form-label">Company Email:</label>
                                                <div class="col-md-8 toolTipCol">
                                                    {{$company->email}}
                                                </div>
                                            </div>
                                            <div class="form-group row align-items-center flex-wrap">
                                                <label class="col-md-4 col-form-label">Website Url:</label>
                                                <div class="col-md-8 toolTipCol">
                                                    {{ $company->website_url }}
                                                </div>
                                            </div>
                                            <div class="text-center">
                                                <a href="{{ url('admin/company-list') }}" class="btn btn-danger">Back</a>
                                            </div>
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                        <!-- /.box-body -->
                    </div>
                </div>
                <!-- /.box -->
            </div>
        </section>
        
    </section>
    <!-- /.content-wrapper -->
</div>
@endsection