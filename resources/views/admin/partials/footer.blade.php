<footer class="main-footer">
  <div class="pull-right hidden-xs">
    
  </div>
  <strong>Copyright &copy; 2019-<?php echo date('Y'); ?> ODC.</strong> All rights
  reserved.
</footer>