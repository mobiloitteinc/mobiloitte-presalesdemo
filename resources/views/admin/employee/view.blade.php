@extends('admin.layouts.master')
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      View Employee
    </h1>
  </section>

   <!-- Main content -->
  <section class="content">

      <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h2 class="box-title"></h2>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
             <div class="container">
               <div class="row">
                <div class="col-sm-10">
                  <div class="profileView bgWhite mb30 ">
                    <br>
                    <div class="form-group row align-items-center flex-wrap">
                        <label class="col-md-4 col-form-label">Name:</label>
                        <div class="col-md-8 toolTipCol">
                           {{$employee->first_name.' '.$employee->last_name}}
                        </div>
                    </div>
                    <div class="form-group row align-items-center flex-wrap">
                        <label class="col-md-4 col-form-label">Email:</label>
                        <div class="col-md-8 toolTipCol">
                           {{$employee->email}}
                        </div>
                    </div>
                    <div class="form-group row align-items-center flex-wrap">
                        <label class="col-md-4 col-form-label">Phone Number:</label>
                        <div class="col-md-8 toolTipCol">
                           {{ $employee->phone }}
                        </div>
                    </div>
                    <div class="form-group row align-items-center flex-wrap">
                        <label class="col-md-4 col-form-label">Company Name:</label>
                        <div class="col-md-8 toolTipCol">
                           {{ $company_name }}
                        </div>
                    </div>
                    <div class="text-center">
                     <a href="{{ url('admin/employee-list') }}" class="btn btn-danger">Back</a>
                    </div>
                  </div> 
                </div>
                   
               </div>
               </div>
             </div>
            <!-- /.box-body -->
           </div>
          </div>
          <!-- /.box -->
        </div>
    </section>
  
</section>
<!-- /.content-wrapper -->
</div>
@endsection
