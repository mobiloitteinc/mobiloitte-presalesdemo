@extends('admin.layouts.master')
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
        Add Employee
        </h1>
    </section>
    <!-- Main content -->
    <section class="content">
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <div class="box-header">
                            <h2 class="box-title"></h2>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                            @endif
                            <div class="container">
                                <div class="row">
                                    <div class="col-sm-10">
                                        <form method="POST" action="{{url('admin/employee-store')}}" id="employee_add" enctype="multipart/form-data">
                                            {{ csrf_field() }}
                                            <div class="profileView bgWhite mb30 ">
                                                <div class="formBox max-WT-600 center-box">
                                                    <br>
                                                    <div class="form-group row align-items-center flex-wrap">
                                                        <label class="col-md-4 col-form-label">First Name</label>
                                                        <div class="col-md-8 toolTipCol">
                                                            <input type="text" class="form-control" name="first_name" value="{{old('first_name')}}" maxlength="199" placeholder="Enter First Name" required>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row align-items-center flex-wrap">
                                                        <label class="col-md-4 col-form-label">Last Name</label>
                                                        <div class="col-md-8 toolTipCol">
                                                            <input type="text" class="form-control" name="last_name" value="{{old('last_name')}}" maxlength="199" placeholder="Enter Last Name" required>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row align-items-center flex-wrap">
                                                        <label class="col-md-4 col-form-label">Company Name</label>
                                                        <div class="col-md-8 toolTipCol">
                                                            
                                                            <select class="form-control" name="company" required>
                                                                <option value="">--Select Company</option>
                                                                @foreach($companies as $key => $company)
                                                                <option value="{{ $company->id }}">{{ $company->name }}</option>
                                                                @endforeach
                                                            </select>
                                                            
                                                        </div>
                                                    </div>
                                                    <div class="form-group row align-items-center flex-wrap {{ $errors->has('company_name') ? ' has-error' : '' }}">
                                                        <div class="form-group row align-items-center flex-wrap">
                                                            <label class="col-md-4 col-form-label">Employee Email</label>
                                                            <div class="col-md-8 toolTipCol">
                                                                <input type="text" class="form-control" name="employee_email" value="{{old('employee_email')}}" maxlength="200" placeholder="Enter Employee Email" required>
                                                            </div>
                                                        </div>
                                                        <div class="form-group row align-items-center flex-wrap {{ $errors->has('employee_email') ? ' has-error' : '' }}">
                                                            <label class="col-md-4 col-form-label">Employee Phone</label>
                                                            <div class="col-md-8 toolTipCol">
                                                                <input type="text" class="form-control" name="employee_phone" value="{{old('employee_phone')}}" maxlength="13" placeholder="Enter Phone Number" required>
                                                            </div>
                                                        </div>
                                                        <div class="form-group row align-items-center flex-wrap {{ $errors->has('employee_phone') ? ' has-error' : '' }}">
                                                            <label class="col-md-4 col-form-label">Employee Password</label>
                                                            <div class="col-md-8 toolTipCol">
                                                                <input type="password" class="form-control" name="password" value="{{old('password')}}" maxlength="16" placeholder="Enter Password" required>
                                                            </div>
                                                        </div>
                                                        <div class="form-group row align-items-center flex-wrap {{ $errors->has('password') ? ' has-error' : '' }}">
                                                            <label class="col-md-4 col-form-label">Confirm Password</label>
                                                            <div class="col-md-8 toolTipCol">
                                                                <input type="password" class="form-control" name="password_confirmation" value="{{old('password_confirmation')}}" maxlength="16" placeholder="Enter Password" required>
                                                            </div>
                                                        </div>
                                                        <div class="form-group row align-items-center flex-wrap {{ $errors->has('password') ? ' has-error' : '' }}">
                                                        </div>
                                                        <div class="text-center">
                                                            <button class="btn btn-primary max-WT-180" type="submit">Add</button>
                                                            <a href="{{ url('admin/employee-list') }}" class="btn btn-danger">Cancel</a>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                            
                                        </div>
                                    </div>
                                </div>
                                <!-- /.box-body -->
                            </div>
                        </div>
                        <!-- /.box -->
                    </div>
                </section>
                
            </section>
            <!-- /.content-wrapper -->
        </div>
        @endsection
        @section('scripts')
        <script type="text/javascript">
        $("#employee_add").validate({
        errorElement: "span",
        wrapper: "span",
        errorPlacement: function(error, element) {
        offset = element.offset();
        error.insertAfter(element)
        error.css('color','red');
        },
        rules: {
            first_name: {
                required: true,
                maxlength: 50
            },
            last_name: {
                required: true,
                maxlength: 50
            },
            employee_email: {
                required: true,
                email: true
            },
            employee_phone: {
                digits:true,
                required:true
            },
            password: {
                required:true
            },
            password_confirmation: {
                required:true
            },
        },
        messages: {
            first_name:{
                required:"*Please enter employee first name.",
                maxlength:"*Please enter upto 50 characters."
            },
            last_name:{
                required:"*Please enter employee last name.",
                maxlength:"*Please enter upto 50 characters."
            },
            employee_email:{
                required:"*Please enter email.",
                email:"*Invalid email."
            },
        },
        });
        </script>
        @endsection