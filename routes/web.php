<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Auth::routes();

Route::prefix('admin')->middleware(['auth', 'back_history'])->group(function () {
 
    Route::get('/logout', function () {
        Auth::logout();
        return Redirect::to('login');
    });
    Route::get('company-add', 'Admin\CompanyController@create');
    Route::get('company-edit/{id}', 'Admin\CompanyController@edit');
    Route::get('company-list', 'Admin\CompanyController@index');
    Route::post('company-store', 'Admin\CompanyController@store');
    Route::post('company-update/{id}', 'Admin\CompanyController@update');
    Route::get('company-delete/{id}', 'Admin\CompanyController@destroy');
    Route::get('company-view/{id}', 'Admin\CompanyController@show');

    Route::get('employee-add', 'Admin\EmployeeController@create');
    Route::get('employee-edit/{id}', 'Admin\EmployeeController@edit');
    Route::get('employee-list', 'Admin\EmployeeController@index');
    Route::post('employee-store', 'Admin\EmployeeController@store');
    Route::post('employee-update/{id}', 'Admin\EmployeeController@update');
    Route::get('employee-delete/{id}', 'Admin\EmployeeController@destroy');
    Route::get('employee-view/{id}', 'Admin\EmployeeController@show');
});
