<?php

namespace App\Http;

use Validator;
use App\Company;

class Helper
{
    /**
     * Check Validator.
     *
     * @param  array $validate $request
     * @return \Illuminate\Http\Response
     */
    public function companyEmailCheck($email)
    {
        if (Company::where('email', $email)->count() > 0) {
            return false;
        } else {
            return true;
        }
    }
}
