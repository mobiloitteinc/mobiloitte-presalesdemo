<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Rules\EmployeeEmailExist;
use App\Company;
use App\User;
use Helper;
use DB;
use Exception;
use Validator;
use Auth;
use Session;
use Storage;
use Toastr;
use Hash;

class EmployeeController extends Controller
{

    /**
     * Helper
     *
     * @var \App\Helper
    */
    protected $helper;

    public function __construct(Helper $helper, Company $company, Toastr $toastr, User $employee)
    {
        $this->helper = $helper;
        $this->company = $company;
        $this->toastr = $toastr;
        $this->employee = $employee;
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $employees = $this->employee->with('companyUser')->where('role_id', '!=', 1)->paginate(10);
        return view('admin.employee.index', compact('employees'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $companies = $this->company->all();
        return view('admin.employee.add', compact('companies'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            [
            'first_name' => ['required'],
            'company' => ['required'],
            'employee_email' => ['required', 'email', new EmployeeEmailExist],
            'employee_phone' => ['required', 'numeric'],
            'password' => 'required|confirmed|min:8|max:16',
            'password_confirmation' => 'required|min:8|max:16'
            ]
        );
        if ($validator->fails()) {
            $message = "Failed to add employee.";
            Toastr::error($message, $title = "Failed", $options = []);
            Toastr::clear();
            return redirect('admin/employee-add')
            ->withErrors($validator)
            ->withInput();
        }
        $employee_data = ['role_id' => 2, 'company_id'=> $request->input('company'), 'email'=> $request->input('employee_email'), 'first_name' => $request->input('first_name'), 'last_name' => $request->input('last_name'), 'phone' => $request->input('employee_phone'), 'password' => Hash::make($request->input('password'))];
        $store_employee_data = $this->employee->create($employee_data);
        if ($store_employee_data) {
            // Show toastr here
            $message = "Employee added successfully!";
            Toastr::success($message, $title = "Added", $options = []);
            Toastr::clear();
            return redirect('admin/employee-list');
        } else {
            $message = "Failed to add employee.";
            Toastr::error($message, $title = "Error", $options = []);
            Toastr::clear();
            return redirect('admin/employee-list');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $employee = $this->employee->find($id);
        $company = $this->company->find($employee->company_id);
        $company_name = $company->name;
        return view('admin.employee.view', compact('employee', 'company_name'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $employee = $this->employee->find($id);
        $companies = $this->company->all();
        return view('admin.employee.edit', compact('employee', 'companies'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make(
            $request->all(),
            [
            'first_name' => ['required'],
            'company' => ['required'],
            'employee_email' => ['required', 'email', 'unique:companies,email,'.$id],
            'employee_phone' => ['required', 'numeric']
            ]
        );
        if ($validator->fails()) {
            $message = "Failed to update employee.";
            Toastr::error($message, $title = "Failed", $options = []);
            Toastr::clear();
            return redirect('admin/employee-add')
            ->withErrors($validator)
            ->withInput();
        }
        $employee_data = ['company_id'=> $request->input('company'), 'email'=> $request->input('employee_email'), 'first_name' => $request->input('first_name'), 'last_name' => $request->input('last_name'), 'phone' => $request->input('employee_phone'), 'password' => Hash::make($request->input('password'))];
        $store_employee_data = $this->employee->where('id', $id)->update($employee_data);
        if ($store_employee_data) {
            // Show toastr here
            $message = "Employee update successfully!";
            Toastr::success($message, $title = "Updated", $options = []);
            Toastr::clear();
            return redirect('admin/employee-list');
        } else {
            $message = "Failed to add employee.";
            Toastr::error($message, $title = "Error", $options = []);
            Toastr::clear();
            return redirect('admin/employee-list');
        }
    }

        /**
        * Remove the specified resource from storage.
        *
        * @param  int  $id
        * @return \Illuminate\Http\Response
        */
    public function destroy($id)
    {
        $delete_company = $this->employee->find($id)->delete();
        return 1;
    }
}
