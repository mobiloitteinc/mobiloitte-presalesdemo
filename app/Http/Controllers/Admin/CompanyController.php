<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Company;
use App\User;
use Helper;
use DB;
use Exception;
use Validator;
use Auth;
use Session;
use Storage;
use Toastr;
use Mail;
use Image;
use App\Mail\EmailNotificationProduct;
use App\Rules\CompanyEmailExist;

class CompanyController extends Controller
{

    /**
     * Helper
     *
     * @var \App\Helper
    */
    protected $helper;

    public function __construct(Helper $helper, Company $company, Toastr $toastr, User $employee)
    {
        $this->helper = $helper;
        $this->employee = $employee;
        $this->company = $company;
        $this->toastr = $toastr;
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $companies = $this->company->paginate(10);
        
        return view('admin.company.index', compact('companies'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.company.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            [
            'company_name' => ['required'],
            'company_email' => ['required', 'email', new CompanyEmailExist],
            'website_url' => 'url',
            ]
        );
        if ($validator->fails()) {
            $message = "Failed to add company.";
            Toastr::error($message, $title = "Failed", $options = []);
            Toastr::clear();
            return redirect('admin/company-add')
            ->withErrors($validator)
            ->withInput();
        }
        $imagePath = '';
        if ($request->file('image')->isValid()) {
            $imagePath = $request->file('image')->store('public');
            $image = Image::make(Storage::get($imagePath))->resize(100, 100)->encode();
            Storage::put($imagePath, $image);
            $imagePath = explode('/', $imagePath);

            $imagePath = $imagePath[1];
        }

        $company_data = ['name'=> $request->input('company_name'), 'email'=> $request->input('company_email'), 'logo' => $imagePath, 'website_url' => $request->input('website_url')];
        $store_company = $this->company->create($company_data);
        if ($store_company) {
            Mail::to(Auth::user()->email)->send(new EmailNotificationProduct());
            // Show toastr here
            $message = "Company added successfully!";
            Toastr::success($message, $title = "Added", $options = []);
            Toastr::clear();
            return redirect('admin/company-list');
        } else {
            $message = "Failed to add company.";
            Toastr::error($message, $title = "Error", $options = []);
            Toastr::clear();
            return redirect('admin/company-list');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $company = $this->company->find($id);
        return view('admin.company.view', compact('company'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $company = $this->company->find($id);
        return view('admin.company.edit', compact('company'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make(
            $request->all(),
            [
            'company_name' => ['required'],
            'company_email' => ['required', 'email', 'unique:companies,email,'.$id],
            'website_url' => 'url',
            ]
        );
        if ($validator->fails()) {
            $message = "Failed to update company.";
            Toastr::error($message, $title = "Failed", $options = []);
            Toastr::clear();
            return redirect('admin/company-edit/'.$id)
            ->withErrors($validator)
            ->withInput();
        }

        $check_email = $this->helper->companyEmailCheck($request->input('company_email'));
        if ($check_email) {
        }

        $company_data = ['name'=> $request->input('company_name'), 'email'=> $request->input('company_email'), 'logo' => $request->input('image'), 'website_url' => $request->input('website_url')];
        $store_company = $this->company->where('id', $id)->update($company_data);
        if ($store_company) {
            // Show toastr here
            $message = "Company updated successfully!";
            Toastr::success($message, $title = "Added", $options = []);
            Toastr::clear();
            return redirect('admin/company-list');
        } else {
            $message = "Failed to update company.";
            Toastr::error($message, $title = "Error", $options = []);
            Toastr::clear();
            return redirect('admin/company-list');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $delete_user_company = $this->employee->where('company_id', $id)->delete();
        $delete_company = $this->company->find($id)->delete();
        return 1;
    }
}
