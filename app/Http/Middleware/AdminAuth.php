<?php

namespace App\Http\Middleware;
use Auth;
use Closure;

class AdminAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

         if(! Auth::check())
            return redirect()->to('admin/login');
         else   
         {   
          /* $user = Auth::user();
            if($user->role_id  == 2){
                return redirect('/');
            }else{*/
              return $next($request);    
           /* }*/
         }
    }
}
