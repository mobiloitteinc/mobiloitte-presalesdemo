<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use App\Company;
use Auth;

class CompanyEmailExist implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        return (empty(Company::where('email', $value)->first()));
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Company email already exist.';
    }
}
