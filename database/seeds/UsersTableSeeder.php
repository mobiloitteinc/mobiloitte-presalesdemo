<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->delete();
        DB::table('users')->insert([
            'email' => 'admin@admin.com',
            'first_name' => 'Admin',
            'phone' => '989901234',
            'password' => bcrypt('password'),
            'role_id' => '1'
        ]);
    }
}
