<?php

use Illuminate\Database\Seeder;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->delete();
        $role = array(
            array(
                'id' => 1,
                'role' => 'admin'
            ),
            array(
                'id' => 2,
                'role' => 'customer'

            ),
        );
        DB::table('roles')->insert($role);
    }
}
