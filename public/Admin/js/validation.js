 $(document).ready(function(){
     var base_url = $('body').attr('data-baseurl');
     var public_url = $('meta[name="base_url"]').attr('content');
     var csrf_token = $('[name="_token"]').val();
    

 /**********Prefix by sapce***********/
$.validator.methods.customValidation = function (value, element) {
    if (/^\s/.test(value) == !0) ab = !1;
    else ab = !0;
    return ab
};


/**********Check Image Extension***********/
$.validator.methods.checkExtension = function (value, element) {

    var res = (/[.]/.exec(value)) ? /[^.]+$/.exec(value) : undefined;
    if (res == 'png' || res == 'jpeg' || res == 'jpg' || res == undefined || res == 'PNG' || res == 'JPEG' || res == 'JPG') {
        return true;
    }else{
        return false;
    }
};

/**********Check Book Extension***********/
$.validator.methods.checkBookExtension = function (value, element) {
    var res = (/[.]/.exec(value)) ? /[^.]+$/.exec(value) : undefined;
    if (res == 'pdf' || res == 'epub' || res == undefined) {
        return true;
    }else{
        return false;
    }
};

/**********Check Contract Extension***********/
$.validator.methods.checkContractExtension = function (value, element) {
    var res = (/[.]/.exec(value)) ? /[^.]+$/.exec(value) : undefined;

    if (res == 'pdf' || res == undefined) {
        return true;
    }else{
        return false;
    }
};

$.validator.methods.emailVal = function(value) {
           var reg = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
            if(reg.test(value) != true)
                ab = false;
            else 
                ab = true;
            return ab; 
        };

$.validator.methods.alphabetsOnly = function (value, element) {
    if (/^[a-zA-Z ]*$/.test(value) != !0) 
        ab = !1;
    else 
        ab = !0;
    return ab
};

$.validator.methods.UrlVal = function (value, element) {
    if (/^(http:\/\/www\.|https:\/\/www\.|http:\/\/|https:\/\/)?[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/.test(value) != !0) 
        ab = !1;
    else 
        ab = !0;
    return ab
};

$.validator.methods.alphaNumericOnly = function (value, element) {
    if (/^(?![0-9]*$)[a-zA-Z0-9' ']+$/i.test(value) != !0) 
        ab = !1;
    else 
        ab = !0;
    return ab
};


$.validator.methods.PhoneValidation = function (value, element) {
    if (/^(?!(\d)\1{9})(?!0123456789|1234567890|0987654321|9876543210)\d{10}$/.test(value) != !0) 
        ab = !1;
    else 
        ab = !0;
    return ab
};


$.validator.methods.PasswordValidation = function (value, element) {

    $(this).blur(function () {
        var pass = $('#password').val();
        ab = !1;
        if (value) {
            if (pass == '') {
                ab = !1;
            } else {
                ab = !0;
            }
        }
        return ab;
    });
};


$.validator.methods.PrefixNotSpace = function (value, element) {
    if (/^\s*[\da-zA-Z][\da-zA-Z\s]*$/.test(value) != !0) 
        ab = !1;
    else 
        ab = !0;
    return ab
};

$.validator.methods.PrefixByZero = function (value, element) {
    if (/^[1-9][0-9]*$/.test(value) != !0) 
        ab = !1;
    else 
        ab = !0;
    return ab
};

$.validator.methods.NoSpace = function (value, element) {
    if (/^\S*$/.test(value) != !0) 
        ab = !1;
    else 
        ab = !0;
    return ab
};

$.validator.methods.NoZeroAllowed = function (value, element) {
    if (/^(?!0+$)\d{10,10}$/.test(value) != !0) 
        ab = !1;
    else 
        ab = !0;
    return ab
};

$.validator.methods.isbnValidation = function (value, element) {
    if (/^(\d+-?)+\d+$/.test(value) != !0) 
        ab = !1;
    else 
        ab = !0;
    return ab
};

$.validator.methods.dateValidation = function (value, element) {
    if (/^([0-9]{2})\/([0-9]{2})\/([0-9]{4})$/.test(value) == !0) 
        ab = !1;
    else 
        ab = !0;
    return ab
};

/************Validate URL***********/
function validateURL(textval) {
    var urlregex = new RegExp("^(http:\/\/www.|https:\/\/www.|ftp:\/\/www.|www.){1}([0-9A-Za-z]+\.)");
    return urlregex.test(textval)
}

/*Image accepted type*/
        $.validator.addMethod( "extension", function( value, element, param ) {
        param = typeof param === "string" ? param.replace( /,/g, "|" ) : "png|jpe?g|pdf|doc|xls|xlsx|odt";
        return this.optional( element ) || value.match( new RegExp( "\\.(" + param + ")$", "i" ) );
        }, $.validator.format( "Only png,jpeg,jpg are allowed." ) );

jQuery.validator.addMethod("greaterThan", 
function(value, element, params) {

    if (!/Invalid|NaN/.test(new Date(value))) {
        return new Date(value) > new Date($(params).val());
    }

    return isNaN(value) && isNaN($(params).val()) 
        || (Number(value) > Number($(params).val())); 
},'Must be greater than {0}.');




/********************zeroNotAllowed Validation*********************/
$.validator.methods.zeroNotAllowed = function (value, element) {
    var str = value;
    var res = str.charAt(0);
    var len = str.length;
    if (str == '0000000000') {
        return !1
    }if (str == '00000000000') {
        return !1
    }if (str == '000000000000') {
        return !1
    }if (str == '0000000000000') {
        return !1
    }if (str == '00000000000000') {
        return !1
    }if (str == '000000000000000') {
        return !1
    }if (str == '0000000000000000') {
        return !1
    }if (str == '00000000000000000') {
        return !1
    }if (str == '000000000000000000') {
        return !1
    }if (str == '0000000000000000000') {
        return !1
    }if (str == '00000000000000000000') {
        return !1
    }else{
        return !0
    }

};


/*Add user form validation*/

$("#add_content_manager").validate({
    rules: {
        name: {
            required: !0,
            minlength: 3,
            maxlength: 60,
            customValidation:!0,
            alphabetsOnly:!0
        },
        unique_user_id: {
            required: !0,
            minlength: 5,
            maxlength: 5,
            customValidation:!0,
            isbnValidation:!0,
            remote: {
               url: public_url+'/superadmin/check-id',
               type: "POST",
               headers: {
                   'X-CSRF-TOKEN': csrf_token
               },
               data: {
                   email: function() {
                       return $( "#email" ).val();
                   }
               },
               dataFilter: function (data) {
                    var json = JSON.parse(data);
                    if (json) {
                        return "\"" + "This id already exists." + "\"";
                    } else {
                        return 'true';
                    }
                }
            }
        },
        email: {
            required: !0,
            minlength: 6,
            maxlength: 70,
            customValidation:!0,
            email:!0,
            emailVal : !0,
            remote: {
               url: public_url+'/superadmin/check-email',
               type: "POST",
               headers: {
                   'X-CSRF-TOKEN': csrf_token
               },
               data: {
                   email: function() {
                       return $( "#email" ).val();
                   }
               },
               dataFilter: function (data) {
                    var json = JSON.parse(data);
                    if (json) {
                        return "\"" + "This email already exists." + "\"";
                    } else {
                        return 'true';
                    }
                }
            }

        },
        gender: {
            required: !0
        },
        password: {
            required: !0,
            customValidation:!0,
            minlength:8,
            maxlength:16
        },
        confirm_password: {
            required: !0,
            customValidation:!0,
            minlength:8,
            maxlength:16,
            equalTo:'#password'
        } 
    },
    messages: {
        name: {
            required: "*Please enter the name.",
            customValidation: "Name should not be prefix by space.",
            alphabetsOnly:'Please enter valid name',
            minlength:"Name contains atleast 3 characters.",
            maxlength:"Name contains maximum 60 characters."
        },
        unique_user_id: {
            required: "*Please enter the unique user id.",
            customValidation: "ID should not be prefix by space.",
            isbnValidation:'Please enter valid ID',
            minlength:"Name contains atleast 5 characters.",
            maxlength:"Name contains maximum 5 characters."
        },
        email: {
            required: "*Please enter the email.",
            minlength: "Please enter atleast 6 characters.",
            maxlength: "Please enter maximum 70 characters.",
            customValidation: "Email should not be prefix by space.",
            email:"Please enter valid email.",
            emailVal : "Please enter valid email."
        },
        gender: {
            required: "*Please select a gender."
        },
        password: {
            required: "*Please enter the password.",
            customValidation: "Password should not be prefix by space.",
            minlength:"Password contains atleast 8 characters.",
            maxlength:"Password contains maximum 16 characters."
        },
        confirm_password: {
            required: "*Please enter the confirm password.",
            customValidation: "Confirm password should not be prefix by space.",
            minlength:"Confirm password contains atleast 8 characters.",
            maxlength:"Confirm password contains maximum 16 characters.",
            equalTo: "Confirm password not match password."
        }
    },
    submitHandler: function (form) {
        abc = 'sdsadsad';
        checkBeginningWhiteSpace(abc)
    }
});

/*Edit user form validation*/

$("#edit_content_manager").validate({
    rules: {
        name: {
            required: !0,
            minlength: 3,
            maxlength: 60,
            customValidation:!0,
            alphabetsOnly:!0
        },
        unique_user_id: {
            required: !0,
            minlength: 5,
            maxlength: 5,
            customValidation:!0,
            isbnValidation:!0
        },
        email: {
            required: !0,
            minlength: 6,
            maxlength: 70,
            customValidation:!0,
            email:!0,
            emailVal : !0
        },
        gender: {
            required: !0
        },
        password: {
            customValidation:!0,
            minlength:8,
            maxlength:16
        },
        confirm_password: {
            customValidation:!0,
            minlength:8,
            maxlength:16,
            equalTo:'#password'
        } 
    },
    messages: {
        name: {
            required: "*Please enter the name.",
            customValidation: "Name should not be prefix by space.",
            alphabetsOnly:'Please enter valid name',
            minlength:"Name contains atleast 3 characters.",
            maxlength:"Name contains maximum 60 characters."
        },
        unique_user_id: {
            required: "*Please enter the unique user id.",
            customValidation: "ID should not be prefix by space.",
            isbnValidation:'Please enter valid ID',
            minlength:"Name contains atleast 5 characters.",
            maxlength:"Name contains maximum 5 characters."
        },
        email: {
            required: "*Please enter the email.",
            minlength: "Please enter atleast 6 characters.",
            maxlength: "Please enter maximum 70 characters.",
            customValidation: "Email should not be prefix by space.",
            email:"Please enter valid email.",
            emailVal : "Please enter valid email."
        },
        gender: {
            required: "*Please select a gender."
        },
        password: {
            customValidation: "Password should not be prefix by space.",
            minlength:"Password contains atleast 8 characters.",
            maxlength:"Password contains maximum 16 characters."
        },
        confirm_password: {
            customValidation: "Confirm password should not be prefix by space.",
            minlength:"Confirm password contains atleast 8 characters.",
            maxlength:"Confirm password contains maximum 16 characters.",
            equalTo: "Confirm password not match password."
        }
    },
    submitHandler: function (form) {
        abc = 'sdsadsad';
        checkBeginningWhiteSpace(abc)
    }
});




});